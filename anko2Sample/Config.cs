﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnkoAnk {
	[Serializable()]
	public class Config {

        public String Ank_Title_S { get; set; }
        public String Ank_Ans_S1 { get; set; }
        public String Ank_Ans_S2 { get; set; }
        public String Ank_Ans_S3 { get; set; }
        public String Ank_Ans_S4 { get; set; }
        public String Ank_Ans_S5 { get; set; }
        public String Ank_Ans_S6 { get; set; }
        public String Ank_Ans_S7 { get; set; }
        public String Ank_Ans_S8 { get; set; }
        public String Ank_Ans_S9 { get; set; }
        public bool Ank_Allow184_Check { get; set; }

		public Config() {
            Ank_Title_S = "";
            Ank_Ans_S1 = "";
            Ank_Ans_S2 = "";
            Ank_Ans_S3 = "";
            Ank_Ans_S4 = "";
            Ank_Ans_S5 = "";
            Ank_Ans_S6 = "";
            Ank_Ans_S7 = "";
            Ank_Ans_S8 = "";
            Ank_Ans_S9 = "";
		}

	}
}
