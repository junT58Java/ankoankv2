﻿namespace AnkoAnk
{
	partial class SampleForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Ank_Start = new System.Windows.Forms.Button();
            this.Ank_Stop = new System.Windows.Forms.Button();
            this.Ank_Title = new System.Windows.Forms.ComboBox();
            this.Ank_Ans1 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans2 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans3 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans4 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans5 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans6 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans7 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans8 = new System.Windows.Forms.ComboBox();
            this.Ank_Ans9 = new System.Windows.Forms.ComboBox();
            this.Ank_Allow184 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "アンケート題名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "回答1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(120, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "回答2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(227, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "回答3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "回答4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(120, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "回答5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(227, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "回答6";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 126);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "回答7";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(120, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "回答8";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(227, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 12);
            this.label10.TabIndex = 9;
            this.label10.Text = "回答9";
            // 
            // Ank_Start
            // 
            this.Ank_Start.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Ank_Start.Location = new System.Drawing.Point(15, 190);
            this.Ank_Start.Name = "Ank_Start";
            this.Ank_Start.Size = new System.Drawing.Size(154, 45);
            this.Ank_Start.TabIndex = 20;
            this.Ank_Start.Text = "アンケート開始";
            this.Ank_Start.UseVisualStyleBackColor = true;
            this.Ank_Start.Click += new System.EventHandler(this.Ank_Start_Click);
            // 
            // Ank_Stop
            // 
            this.Ank_Stop.Enabled = false;
            this.Ank_Stop.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Bold);
            this.Ank_Stop.Location = new System.Drawing.Point(178, 189);
            this.Ank_Stop.Name = "Ank_Stop";
            this.Ank_Stop.Size = new System.Drawing.Size(153, 46);
            this.Ank_Stop.TabIndex = 21;
            this.Ank_Stop.Text = "アンケート停止";
            this.Ank_Stop.UseVisualStyleBackColor = true;
            this.Ank_Stop.Click += new System.EventHandler(this.Ank_Stop_Click);
            // 
            // Ank_Title
            // 
            this.Ank_Title.FormattingEnabled = true;
            this.Ank_Title.Location = new System.Drawing.Point(15, 28);
            this.Ank_Title.Name = "Ank_Title";
            this.Ank_Title.Size = new System.Drawing.Size(314, 20);
            this.Ank_Title.TabIndex = 22;
            // 
            // Ank_Ans1
            // 
            this.Ank_Ans1.FormattingEnabled = true;
            this.Ank_Ans1.Location = new System.Drawing.Point(15, 66);
            this.Ank_Ans1.Name = "Ank_Ans1";
            this.Ank_Ans1.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans1.TabIndex = 23;
            // 
            // Ank_Ans2
            // 
            this.Ank_Ans2.FormattingEnabled = true;
            this.Ank_Ans2.Location = new System.Drawing.Point(122, 66);
            this.Ank_Ans2.Name = "Ank_Ans2";
            this.Ank_Ans2.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans2.TabIndex = 24;
            // 
            // Ank_Ans3
            // 
            this.Ank_Ans3.FormattingEnabled = true;
            this.Ank_Ans3.Location = new System.Drawing.Point(229, 66);
            this.Ank_Ans3.Name = "Ank_Ans3";
            this.Ank_Ans3.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans3.TabIndex = 25;
            // 
            // Ank_Ans4
            // 
            this.Ank_Ans4.FormattingEnabled = true;
            this.Ank_Ans4.Location = new System.Drawing.Point(15, 104);
            this.Ank_Ans4.Name = "Ank_Ans4";
            this.Ank_Ans4.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans4.TabIndex = 26;
            // 
            // Ank_Ans5
            // 
            this.Ank_Ans5.FormattingEnabled = true;
            this.Ank_Ans5.Location = new System.Drawing.Point(122, 104);
            this.Ank_Ans5.Name = "Ank_Ans5";
            this.Ank_Ans5.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans5.TabIndex = 27;
            // 
            // Ank_Ans6
            // 
            this.Ank_Ans6.FormattingEnabled = true;
            this.Ank_Ans6.Location = new System.Drawing.Point(229, 104);
            this.Ank_Ans6.Name = "Ank_Ans6";
            this.Ank_Ans6.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans6.TabIndex = 28;
            // 
            // Ank_Ans7
            // 
            this.Ank_Ans7.FormattingEnabled = true;
            this.Ank_Ans7.Location = new System.Drawing.Point(15, 141);
            this.Ank_Ans7.Name = "Ank_Ans7";
            this.Ank_Ans7.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans7.TabIndex = 29;
            // 
            // Ank_Ans8
            // 
            this.Ank_Ans8.FormattingEnabled = true;
            this.Ank_Ans8.Location = new System.Drawing.Point(122, 141);
            this.Ank_Ans8.Name = "Ank_Ans8";
            this.Ank_Ans8.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans8.TabIndex = 30;
            // 
            // Ank_Ans9
            // 
            this.Ank_Ans9.FormattingEnabled = true;
            this.Ank_Ans9.Location = new System.Drawing.Point(229, 141);
            this.Ank_Ans9.Name = "Ank_Ans9";
            this.Ank_Ans9.Size = new System.Drawing.Size(101, 20);
            this.Ank_Ans9.TabIndex = 31;
            // 
            // Ank_Allow184
            // 
            this.Ank_Allow184.AutoSize = true;
            this.Ank_Allow184.Location = new System.Drawing.Point(15, 168);
            this.Ank_Allow184.Name = "Ank_Allow184";
            this.Ank_Allow184.Size = new System.Drawing.Size(119, 16);
            this.Ank_Allow184.TabIndex = 32;
            this.Ank_Allow184.Text = "184での投票を許可";
            this.Ank_Allow184.UseVisualStyleBackColor = true;
            // 
            // SampleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 246);
            this.Controls.Add(this.Ank_Allow184);
            this.Controls.Add(this.Ank_Ans9);
            this.Controls.Add(this.Ank_Ans8);
            this.Controls.Add(this.Ank_Ans7);
            this.Controls.Add(this.Ank_Ans6);
            this.Controls.Add(this.Ank_Ans5);
            this.Controls.Add(this.Ank_Ans4);
            this.Controls.Add(this.Ank_Ans3);
            this.Controls.Add(this.Ank_Ans2);
            this.Controls.Add(this.Ank_Ans1);
            this.Controls.Add(this.Ank_Title);
            this.Controls.Add(this.Ank_Stop);
            this.Controls.Add(this.Ank_Start);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(359, 285);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(359, 285);
            this.Name = "SampleForm";
            this.ShowIcon = false;
            this.Text = "アンコちゃん擬似アンケート";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SampleForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button Ank_Start;
        private System.Windows.Forms.Button Ank_Stop;
        private System.Windows.Forms.ComboBox Ank_Title;
        private System.Windows.Forms.ComboBox Ank_Ans1;
        private System.Windows.Forms.ComboBox Ank_Ans2;
        private System.Windows.Forms.ComboBox Ank_Ans3;
        private System.Windows.Forms.ComboBox Ank_Ans4;
        private System.Windows.Forms.ComboBox Ank_Ans5;
        private System.Windows.Forms.ComboBox Ank_Ans6;
        private System.Windows.Forms.ComboBox Ank_Ans7;
        private System.Windows.Forms.ComboBox Ank_Ans8;
        private System.Windows.Forms.ComboBox Ank_Ans9;
        private System.Windows.Forms.CheckBox Ank_Allow184;

    }
}