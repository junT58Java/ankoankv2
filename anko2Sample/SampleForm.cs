﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

namespace AnkoAnk {
	internal sealed partial class SampleForm : Form {
		private ankoPlugin2.IPluginHost _host = null;
        private Form _hostForm = null;
        private bool pluginrun = false;
        private Config _configData = new Config();
        private List<int> answer = new List<int>();
        private List<String> user = new List<String>();
        private int count = 0;

        public bool IsRun()
        {
            return pluginrun;
        }

		public SampleForm(ankoPlugin2.IPluginHost host) {
			InitializeComponent();
            _host = host;
            this._hostForm = (Form)host.Win32WindowOwner;
            bool cfg = ConfigLoad();
            Console.WriteLine(cfg + "");
            _host.ReceiveChat += new EventHandler<ankoPlugin2.ReceiveChatEventArgs>(_host_ReceiveChat);
		}

        public void _host_ReceiveChat(object sender, ankoPlugin2.ReceiveChatEventArgs e)
        {
            if (!e.Chat.IsCaster)
            {
                if (IsRun())
                {
                    bool is184 = false;
                    try
                    {
                        Int32.Parse(e.Chat.UserId);
                    }
                    catch
                    {
                        is184 = true;
                    }
                    if (is184 == true && Ank_Allow184.Checked != true)
                    {
                        _host.PostOwnerComment(">>" + e.Chat.No + " このアンケートは184で投票することが許可されていません。<br>184を外して再度投票してください。", "", "Auto Enquete");
                        return;
                    }
                    String convzenkakuintcmnt = str123ToHankaku(e.Chat.Message);
                    bool isnumber = true;
                    int num = 0;
                    try
                    {
                        num = Int32.Parse(convzenkakuintcmnt);
                    }
                    catch
                    {
                        isnumber = false;
                    }
                    if (isnumber)
                    {
                        if (count < num)
                        {
                            _host.PostOwnerComment(">>" + e.Chat.No + " " + num + "番の回答は存在しません。<br>1～" + count + "までの数字で回答してください。", "", "Auto Enquete");
                            return;
                        }
                        if (user.Contains(e.Chat.UserId))
                        {
                            _host.PostOwnerComment(">>" + e.Chat.No + " すでに投票済みです", "", "Auto Enquete");
                            return;
                        }
                        user.Add(e.Chat.UserId);
                        answer.Add(num);
                        _host.PostOwnerComment(">>" + e.Chat.No + " 投票完了", "", "Auto Enquete");
                    }
                }
            }
        }

        string str123ToHankaku(string s)
        {

            Regex re = new Regex("[０-９]+");
            string output = re.Replace(s, myReplacer);
            return output;
        }

        string myReplacer(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Narrow, 0);
        }

        private void SampleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsRun())
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                ConfigSave();
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    e.Cancel = true;
                    this.Hide();
                }
            }
        }

        private void AddQuestion()
        {
            if (Ank_Title.Text != "")
            {
                if (!Ank_Title.Items.Contains(Ank_Title.Text))
                {
                    Ank_Title.Items.Add(Ank_Title.Text);
                }
            }
            if (Ank_Ans1.Text != "")
            {
                if (!Ank_Ans1.Items.Contains(Ank_Ans1.Text))
                {
                    Ank_Ans1.Items.Add(Ank_Ans1.Text);
                }
            }
            if (Ank_Ans2.Text != "")
            {
                if (!Ank_Ans2.Items.Contains(Ank_Ans2.Text))
                {
                    Ank_Ans2.Items.Add(Ank_Ans2.Text);
                }
            }
            if (Ank_Ans3.Text != "")
            {
                if (!Ank_Ans3.Items.Contains(Ank_Ans3.Text))
                {
                    Ank_Ans3.Items.Add(Ank_Ans3.Text);
                }
            }
            if (Ank_Ans4.Text != "")
            {
                if (!Ank_Ans4.Items.Contains(Ank_Ans4.Text))
                {
                    Ank_Ans4.Items.Add(Ank_Ans4.Text);
                }
            }
            if (Ank_Ans5.Text != "")
            {
                if (!Ank_Ans5.Items.Contains(Ank_Ans5.Text))
                {
                    Ank_Ans5.Items.Add(Ank_Ans5.Text);
                }
            }
            if (Ank_Ans6.Text != "")
            {
                if (!Ank_Ans6.Items.Contains(Ank_Ans6.Text))
                {
                    Ank_Ans6.Items.Add(Ank_Ans6.Text);
                }
            }
            if (Ank_Ans7.Text != "")
            {
                if (!Ank_Ans7.Items.Contains(Ank_Ans7.Text))
                {
                    Ank_Ans7.Items.Add(Ank_Ans7.Text);
                }
            }
            if (Ank_Ans8.Text != "")
            {
                if (!Ank_Ans8.Items.Contains(Ank_Ans8.Text))
                {
                    Ank_Ans8.Items.Add(Ank_Ans8.Text);
                }
            }
            if (Ank_Ans9.Text != "")
            {
                if (!Ank_Ans9.Items.Contains(Ank_Ans9.Text))
                {
                    Ank_Ans9.Items.Add(Ank_Ans9.Text);
                }
            }
        }

        private void Ank_Start_Click(object sender, EventArgs e)
        {
            int count = getAnswerCount();
            this.count = count;
            if (Ank_Title.Text == "" || Ank_Ans1.Text == "" || Ank_Ans2.Text == "")
            {
                MessageBox.Show("少なくとも題名、回答１、回答２を入力する必要があります。", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Ank_Start.Enabled = false;
            Ank_Stop.Enabled = true;
            pluginrun = true;
            foreach (string current in getQuestionComment(Ank_Title.Text, count))
            {
                _host.PostOwnerComment(current, "", "Auto Enquete");
            }
            AddQuestion();
            Ank_Title.Enabled = false;
            Ank_Ans1.Enabled = false;
            Ank_Ans2.Enabled = false;
            Ank_Ans3.Enabled = false;
            Ank_Ans4.Enabled = false;
            Ank_Ans5.Enabled = false;
            Ank_Ans6.Enabled = false;
            Ank_Ans7.Enabled = false;
            Ank_Ans8.Enabled = false;
            Ank_Ans9.Enabled = false;
            Ank_Allow184.Enabled = false;
        }

        private string kirisute(double moto)
        {
            string kirisute1 = moto.ToString();
            string kirisute2 = "";
            if (kirisute1.IndexOf('.') != -1)
            {
                string ten = kirisute1.Substring(kirisute1.IndexOf('.') + 1, 1);
                kirisute2 = kirisute1.Substring(0, kirisute1.IndexOf('.')) + "." + ten;
            }
            else
            {
                kirisute2 = kirisute1 + ".0";
            }
            return kirisute2;
        }

        private void PostResult()
        {
            double allcount = answer.Count;
            double s1 = 0;
            double s2 = 0;
            double s3 = 0;
            double s4 = 0;
            double s5 = 0;
            double s6 = 0;
            double s7 = 0;
            double s8 = 0;
            double s9 = 0;
            foreach (int current in answer)
            {
                if (current == 1)
                {
                    s1++;
                }
                else if (current == 2)
                {
                    s2++;
                }
                else if (current == 3)
                {
                    s3++;
                }
                else if (current == 4)
                {
                    s4++;
                }
                else if (current == 5)
                {
                    s5++;
                }
                else if (current == 6)
                {
                    s6++;
                }
                else if (current == 7)
                {
                    s7++;
                }
                else if (current == 8)
                {
                    s8++;
                }
                else if (current == 9)
                {
                    s9++;
                }
            }
            double bp1 = s1 / allcount * 100;
            double bp2 = s2 / allcount * 100;
            double bp3 = s3 / allcount * 100;
            double bp4 = s4 / allcount * 100;
            double bp5 = s5 / allcount * 100;
            double bp6 = s6 / allcount * 100;
            double bp7 = s7 / allcount * 100;
            double bp8 = s8 / allcount * 100;
            double bp9 = s9 / allcount * 100;
            string p1 = kirisute(bp1);
            string p2 = kirisute(bp2);
            string p3 = kirisute(bp3);
            string p4 = kirisute(bp4);
            string p5 = kirisute(bp5);
            string p6 = kirisute(bp6);
            string p7 = kirisute(bp7);
            string p8 = kirisute(bp8);
            string p9 = kirisute(bp9);
            foreach (string cmnt in getResultComment(Ank_Title.Text, p1, p2, p3, p4, p5, p6, p7, p8, p9))
            {
                _host.PostOwnerComment(cmnt, "", "Auto Enquete");
            }
        }

        private void Ank_Stop_Click(object sender, EventArgs e)
        {
            PostResult();
            user = new List<String>();
            answer = new List<int>();
            Ank_Start.Enabled = true;
            Ank_Stop.Enabled = false;
            pluginrun = false;
            Ank_Title.Enabled = true;
            Ank_Ans1.Enabled = true;
            Ank_Ans2.Enabled = true;
            Ank_Ans3.Enabled = true;
            Ank_Ans4.Enabled = true;
            Ank_Ans5.Enabled = true;
            Ank_Ans6.Enabled = true;
            Ank_Ans7.Enabled = true;
            Ank_Ans8.Enabled = true;
            Ank_Ans9.Enabled = true;
            Ank_Allow184.Enabled = true;
        }

        /*
         * 結果表示時に送信するコメントを取得します。
         */
        private List<String> getResultComment(String title, string p1, string p2, string p3, string p4, string p5, string p6, string p7, string p8, string p9)
        {
            List<String> list = new List<String>();
            list.Add("アンケートが終了しました。");
            String cmnt = title + "<br>";
            if (count == 2)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "%";
            }
            else if (count == 3)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%";
            }
            else if (count == 4)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "%";
            }
            else if (count == 5)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "% ⑤" + p5 + "%";
            }
            else if (count == 6)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "% ⑤" + p5 + "% ⑥" + p6 + "%";
            }
            else if (count == 7)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "% ⑤" + p5 + "% ⑥" + p6 + "%<br>⑦" + p7 + "%";
            }
            else if (count == 8)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "% ⑤" + p5 + "% ⑥" + p6 + "%<br>⑦" + p7 + "% ⑧" + p8 + "%";
            }
            else if (count == 9)
            {
                cmnt = cmnt + "①" + p1 + "% ②" + p2 + "% ③" + p3 + "%<br>④" + p4 + "% ⑤" + p5 + "% ⑥" + p6 + "%<br>⑦" + p7 + "% ⑧" + p8 + "% ⑨" + p9 + "%";
            }
            list.Add(cmnt);
            return list;
        }

        /*
         * 質問時に送信するコメントを取得します。
         */
        private List<String> getQuestionComment(String title, int count)
        {
            List<String> list = new List<String>();
            list.Add("アンケートが開始されました。");
            list.Add("1～" + count + "の数字をコメントしてください。");
            if (Ank_Allow184.Checked)
            {
                list.Add("このアンケートは184でも投票ができます。");
            }
            String cmnt = "/perm " + title + "<br>";
            if (count == 2)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text;
            }
            else if (count == 3)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text;
            }
            else if (count == 4)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text;
            }
            else if (count == 5)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text + " ⑤" + Ank_Ans5.Text;
            }
            else if (count == 6)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text + " ⑤" + Ank_Ans5.Text + " ⑥" + Ank_Ans6.Text;
            }
            else if (count == 7)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text + " ⑤" + Ank_Ans5.Text + " ⑥" + Ank_Ans6.Text + "<br>⑦" + Ank_Ans7.Text;
            }
            else if (count == 8)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text + " ⑤" + Ank_Ans5.Text + " ⑥" + Ank_Ans6.Text + "<br>⑦" + Ank_Ans7.Text + " ⑧" + Ank_Ans8.Text;
            }
            else if (count == 9)
            {
                cmnt = cmnt + "①" + Ank_Ans1.Text + " ②" + Ank_Ans2.Text + " ③" + Ank_Ans3.Text + "<br>④" + Ank_Ans4.Text + " ⑤" + Ank_Ans5.Text + " ⑥" + Ank_Ans6.Text + "<br>⑦" + Ank_Ans7.Text + " ⑧" + Ank_Ans8.Text + " ⑨" + Ank_Ans9.Text;
            }
            list.Add(cmnt);
            return list;
        }

        /*
         * 回答の数をInt型で返します。
         */
        private int getAnswerCount()
        {
            int a = 0;
            if (Ank_Ans1.Text != "")
            {
                a = 1;
            }
            if (Ank_Ans2.Text != "" && a == 1)
            {
                a = 2;
            }
            if (Ank_Ans3.Text != "" && a == 2)
            {
                a = 3;
            }
            if (Ank_Ans4.Text != "" && a == 3)
            {
                a = 4;
            }
            if (Ank_Ans5.Text != "" && a == 4)
            {
                a = 5;
            }
            if (Ank_Ans6.Text != "" && a == 5)
            {
                a = 6;
            }
            if (Ank_Ans7.Text != "" && a == 6)
            {
                a = 7;
            }
            if (Ank_Ans8.Text != "" && a == 7)
            {
                a = 8;
            }
            if (Ank_Ans9.Text != "" && a == 8)
            {
                a = 9;
            }
            return a;
        }

        private Config GetConfigData()
        {
            if (this.InvokeRequired)
            {
                return (Config)Invoke(new Func<Config>(GetConfigData));
            }
            String st = "";
            String s1 = "";
            String s2 = "";
            String s3 = "";
            String s4 = "";
            String s5 = "";
            String s6 = "";
            String s7 = "";
            String s8 = "";
            String s9 = "";
            foreach(string current in Ank_Title.Items)
            {
                if (st == "")
                {
                    st = current;
                }
                else
                {
                    st = st + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans1.Items)
            {
                if (s1 == "")
                {
                    s1 = current;
                }
                else
                {
                    s1 = s1 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans2.Items)
            {
                if (s2 == "")
                {
                    s2 = current;
                }
                else
                {
                    s2 = s2 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans3.Items)
            {
                if (s3 == "")
                {
                    s3 = current;
                }
                else
                {
                    s3 = s3 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans4.Items)
            {
                if (s4 == "")
                {
                    s4 = current;
                }
                else
                {
                    s4 = s4 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans5.Items)
            {
                if (s5 == "")
                {
                    s5 = current;
                }
                else
                {
                    s5 = s5 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans6.Items)
            {
                if (s6 == "")
                {
                    s6 = current;
                }
                else
                {
                    s6 = s6 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans7.Items)
            {
                if (s7 == "")
                {
                    s7 = current;
                }
                else
                {
                    s7 = s7 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans8.Items)
            {
                if (s8 == "")
                {
                    s8 = current;
                }
                else
                {
                    s8 = s8 + "§_§" + current;
                }
            }
            foreach (string current in Ank_Ans9.Items)
            {
                if (s9 == "")
                {
                    s9 = current;
                }
                else
                {
                    s9 = s9 + "§_§" + current;
                }
            }
            _configData.Ank_Title_S = st;
            _configData.Ank_Ans_S1 = s1;
            _configData.Ank_Ans_S2 = s2;
            _configData.Ank_Ans_S3 = s3;
            _configData.Ank_Ans_S4 = s4;
            _configData.Ank_Ans_S5 = s5;
            _configData.Ank_Ans_S6 = s6;
            _configData.Ank_Ans_S7 = s7;
            _configData.Ank_Ans_S8 = s8;
            _configData.Ank_Ans_S9 = s9;
            _configData.Ank_Allow184_Check = Ank_Allow184.Checked;
            return _configData;
        }

        /// <summary>
        private string GetConfigXmlPath()
        {
            return Path.Combine(_host.ApplicationDataFolder, this.GetType().Namespace + ".xml");
        }

        private bool ConfigLoad()
        {
            bool result = false;
            Config configData = new Config();

            string configPath = GetConfigXmlPath();
            if (File.Exists(configPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                try
                {
                    using (FileStream fs = new FileStream(configPath, FileMode.Open))
                    {
                        configData = (Config)serializer.Deserialize(fs);
                        result = true;
                        fs.Close();
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            string[] delimiter = { "§_§" };
            foreach (string current in configData.Ank_Title_S.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Title.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S1.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans1.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S2.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans2.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S3.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans3.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S4.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans4.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S5.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans5.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S6.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans6.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S7.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans7.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S8.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans8.Items.Add(current);
            }
            foreach (string current in configData.Ank_Ans_S9.Split(delimiter, StringSplitOptions.RemoveEmptyEntries))
            {
                Ank_Ans9.Items.Add(current);
            }
            Ank_Allow184.Checked = configData.Ank_Allow184_Check;
            return result;
        }

        private bool ConfigSave()
        {
            Config configData = GetConfigData();
            string configPath = GetConfigXmlPath();
            XmlSerializer serializer = new XmlSerializer(typeof(Config));
            using (FileStream fs = new FileStream(configPath, FileMode.Create))
            {
                serializer.Serialize(fs, configData);
                fs.Close();
            }
            return true;
        }

	}
}
